package Lab1.task1;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class Transforms2D extends JPanel {
    private static final int numberOfCorners = 9;

    private class Display extends JPanel {
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            g2.translate(300, 300);  // Moves (0,0) to the center of the display.

            int whichTransform = transformSelect.getSelectedIndex();

            switch (whichTransform) {
                case 1:
                    g2.scale(0.5, 0.5);
                    break;
                case 2:
                    g2.rotate(Math.toRadians(45));
                    break;
                case 3:
                    g2.rotate(Math.toRadians(180));
                    g2.scale(-0.5, 1);
                    break;
                case 4:
                    g2.shear(0.25, 0);
                    break;
                case 5:
                    g2.translate(0, -250);
                    g2.scale(1, 0.5);
                    break;
                case 6:
                    g2.rotate(Math.toRadians(90));
                    g2.shear(0.5, 0);
                    break;
                case 7:
                    g2.rotate(Math.toRadians(180));
                    g2.scale(0.5, 1);
                    break;
                case 8:
                    g2.scale(1, 0.25);
                    g2.translate(0, 800);
                    g2.rotate(Math.toRadians(45));
                    g2.shear(0.9, 0);
                    break;
                case 9:
                    g2.shear(0, 0.25);
                    g2.translate(120, 0);
                    g2.rotate(Math.toRadians(180));
                    break;
            }

            g2.draw(regularPolygon);
        }
    }

    private Display display;
    private BufferedImage pic;
    private JComboBox<String> transformSelect;
    private Polygon regularPolygon;

    public Transforms2D() throws IOException {
        pic = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResource("shuttle.jpg")));

        display = new Display();
        display.setBackground(Color.YELLOW);
        display.setPreferredSize(new Dimension(600, 600));

        transformSelect = new JComboBox<>();
        transformSelect.addItem("None");

        for (int i = 1; i < 10; i++) {
            transformSelect.addItem("No. " + i);
        }

        regularPolygon = new Polygon();
        for (int i = 0; i < numberOfCorners; i++)
            regularPolygon.addPoint((int) (90 * Math.cos(i * 2 * Math.PI / numberOfCorners)),
                    (int) (90 * Math.sin(i * 2 * Math.PI / numberOfCorners)));

        initLayouts();
    }

    private void initLayouts() {
        transformSelect.addActionListener(e -> display.repaint());
        setLayout(new BorderLayout(3, 3));
        setBackground(Color.GRAY);
        setBorder(BorderFactory.createLineBorder(Color.GRAY, 10));
        JPanel top = new JPanel();
        top.setLayout(new FlowLayout(FlowLayout.CENTER));
        top.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        top.add(new JLabel("Transform: "));
        top.add(transformSelect);
        add(display, BorderLayout.CENTER);
        add(top, BorderLayout.NORTH);
    }
}