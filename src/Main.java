import Lab1.task1.Transforms2D;
import Lab1.task2.TransformedShapes;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Main {

    /* Zadanie nr 1 - 9 wierzchołków
       Zadanie nr 2 - 1 figura
    */


    public static void main(String[] args) throws IOException {
        task1();
        /*task2();*/
    }

    private static void task1() throws IOException {
        JFrame window = new JFrame("2D Transforms");
        window.setContentPane(new Transforms2D());
        window.pack();
        window.setResizable(false);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        window.setLocation((screen.width - window.getWidth()) / 2, (screen.height - window.getHeight()) / 2);
        window.setVisible(true);
    }

    private static void task2() {
        JFrame window = new JFrame("Drawing With Transforms");
        window.setContentPane(new TransformedShapes());
        window.pack();
        window.setResizable(false);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        window.setLocation( (screen.width - window.getWidth())/2, (screen.height - window.getHeight())/2 );
        window.setVisible(true);
    }
}
